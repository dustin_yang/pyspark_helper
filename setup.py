from setuptools import setup

setup(
    name='pyspark_helper',
    version='0.0',
    description='Functions to make working with PySpark on Databricks easier',
    url='https://bitbucket.org/dustin_yang/pyspark_helper',
    author='Dustin Yang',
    author_email='dustin.yang@tyme.com',
    packages=['pyspark_helper', 'pyspark_helper.globals', 'pyspark_helper.io', 'pyspark_helper.sdf_ops'],
    install_requires=['pyspark>=0.0'],
    zip_safe=False
)