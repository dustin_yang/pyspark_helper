from pyspark.dbutils import DBUtils

def get_globals(spark, 
                snowflake_secret_scope, 
                microservice_secret_scope):
    dbutils=DBUtils(spark)
  
    snowflake_username = dbutils.secrets.get(snowflake_secret_scope, 'username')
    snowflake_password = dbutils.secrets.get(snowflake_secret_scope, 'password')
    microservice_username = dbutils.secrets.get(microservice_secret_scope, 'username')
    microservice_password = dbutils.secrets.get(microservice_secret_scope, 'password')

    snowflake_options = dict(
        sfUrl='tymebankza.eu-west-1.snowflakecomputing.com',
        sfUser=snowflake_username,
        sfPassword=snowflake_password,
        sfDatabase='DATAMARTS',
        sfSchema='PUBLIC',
        sfWarehouse='CREDIT',
        sfRole='EDW'
    )
  
    microservice_connection_properties = dict(
        user=microservice_username, # consider making a secret
        password=microservice_password, # consider making a secret
        driver='com.mysql.jdbc.Driver'
    )

    globals_ = dict(
        spark_context=spark,
        snowflake_options=snowflake_options,
        microservice_connection_properties=microservice_connection_properties
    )
  
    return globals_