def to_snowflake(globals_, table=None, table_name=None, write_mode='append', **kwargs):
	snowflake_options = kwargs.get('snowflake_options', globals_['snowflake_options'])

	(
		table
		.write
		.format('snowflake')
		.options(**snowflake_options)
		.option('dbtable', table_name)
		.mode(write_mode)
		.save()
	)

	return None
    
def to_databricks_table(df, table_name, write_mode='append'):
    if write_mode == 'append':
        (
            df
            .write
            .format('delta')
            .mode('append')
            .saveAsTable(table_name)
        )
    else:
        (
            df
            .write
            .format('delta')
            .option('overwriteSchema', 'true')
            .mode('overwrite')
            .saveAsTable(table_name)
        )

    return None