import re
import pyspark.sql.functions as f

def read_snowflake(globals_, table_name=None, **kwargs):
    spark_context = kwargs.get('spark_context', globals_['spark_context'])
    snowflake_options = kwargs.get('snowflake_options', globals_['snowflake_options'])
  
    table = (
        spark_context
        .read
        .format('snowflake')
        .options(**snowflake_options)
        .option('dbtable', table_name)
        .load()
    )
    
    for dtype in table.dtypes:
        if re.match('^decimal', dtype[1]):
            table = (
                table
                .withColumn(dtype[0], f.col(dtype[0]).cast('double'))
            )

    return table

def read_microservice(globals_, table_name=None, hostname=None, database=None, port=3306, cols_to_upper=True, *, hostname_suffix='.cluster-ro-c3fo8p1fc4lz.eu-west-1.rds.amazonaws.com', **kwargs):
     spark_context = kwargs.get('spark_context', globals_['spark_context'])
     microservice_connection_properties = kwargs.get('microservice_connection_properties', globals_['microservice_connection_properties'])
     
     url = (
         'jdbc:mysql://{0}:{1}/{2}'
         .format(hostname + hostname_suffix,
                 port,
                 database)
     )
     
     table = (
         spark_context
         .read
         .jdbc(url=url, table=table_name, properties=microservice_connection_properties)
     )
     
     if cols_to_upper:
         table = table.toDF(*[c.upper() for c in table.columns])
         
     return table 
     
def read_databricks_table(globals_, table_name, capitalise_columns=True, **kwargs):
    spark_context = kwargs.get('spark_context', globals_['spark_context'])
    
    table = (
        spark_context
        .table(table_name)
    )

    if capitalise_columns:
        table = table.toDF(*[col.upper() for col in table.columns])

    return table